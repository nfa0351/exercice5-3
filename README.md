# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 
## Objectifs
* Mises en application :
- [x] (Exercice 1) Service REST : Lecture d'un Badge
- [x] (Exercice 2) Service REST : suppression d'un Badge
- [x] **(Exercice 3) Service REST : Ajout d'un Badge**
- [ ] (Exercice 4) "Défragmentation" : Nettoyage des lignes supprimées par reconstitution d'une base propre

----

Afin d'implémenter le service de suppression de badge, abordons le problème par impacts, couche par couche.

## Impacts

### Impact sur le Modèle

### Impact sur la désérialisation des Métadonnées

### Impact sur la sérialisation
      
### Impact sur Les DAOs
     
### Impact sur la couche de Service

## Tests

### Unitaires

### D'acceptance (de recette)

-[ ] *Note: Pour information les images put1/2/3.png ne sont pas présentes dans le répertoire screenshots, les illustrations ne s'affichent pas.*

1. Ajout d'un badge avec valeurs par défaut
![](screenshots/put1.png)
2. Retour du service
![](screenshots/put2.png)
- On observe bien un code de retour 201, avec le lien vers l'instance créée
3. Consultation de l'instance créée
![](screenshots/put3.png)
- On observe bien l'objet demandé
4. Retour du service avec modification de la description
![](screenshots/put4.png)
- On observe bien le code retour 200
5. COnsultation de l'instance modifiée
![](screenshots/put5.png)
   - On observe bien la modification

-[ ] *Pour ce dernier exercice, j'ai tenté le lancement de BadgesServicesApplication sur un autre PC sans faire de mise à jour de module, et avec une version de JDK antérieure mais rien n'y fait ça ne se lance toujours pas :'( :*
  
```java
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v2.5.4)

2022-12-23 12:23:02.640  INFO 6932 --- [           main] f.c.f.n.badges.BadgesServiceApplication  : Starting BadgesServiceApplication using Java 16.0.1 on THIERRYV-W10A with PID 6932 (C:\NFA035\exercice19\exercice-3\badges-service\target\classes started by thierryv in C:\NFA035\exercice19\exercice-3)
2022-12-23 12:23:02.643  INFO 6932 --- [           main] f.c.f.n.badges.BadgesServiceApplication  : No active profile set, falling back to default profiles: default
2022-12-23 12:23:04.236  INFO 6932 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2022-12-23 12:23:04.254  INFO 6932 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2022-12-23 12:23:04.255  INFO 6932 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.52]
2022-12-23 12:23:04.408  INFO 6932 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2022-12-23 12:23:04.408  INFO 6932 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1650 ms
2022-12-23 12:23:06.156  INFO 6932 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2022-12-23 12:23:06.165  INFO 6932 --- [           main] f.c.f.n.badges.BadgesServiceApplication  : Started BadgesServiceApplication in 4.082 seconds (JVM running for 4.732)
2022-12-23 12:23:19.540  INFO 6932 --- [nio-8080-exec-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring DispatcherServlet 'dispatcherServlet'
2022-12-23 12:23:19.540  INFO 6932 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : Initializing Servlet 'dispatcherServlet'
2022-12-23 12:23:19.541  INFO 6932 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : Completed initialization in 1 ms


```
![web][]

-[ ] *Les tests unitaires passent tous bien (sauf celui qui n'a jamais marché) :*
![alltests][]

-[ ] *L'application graphique se lance elle aussi sans problème :*
 ![walletapp][]

**FIN**

----

## Ressources supplémentaires


## Défragmentation  (REMIS à PLUS TARD => non noté pour le moment, mais sujet d'examen blanc potentiel)

- [ ] Développer un traitement "batch" permettant de nettoyer le fichier de base de données Json suite à un certain nombre de suppressions
    - [ ] Il s'agit de lire le fichier de base json octet par octet en sautant les lignes supprimées (comme nous en connaissons les positions), et d'écrire cela dans la foulée vers un nouveau fichier, temporaire
    - [ ] En post-traitement il conviendra d'archiver la base originale, puis de renommer le fichier temporaire du nom du fichier original


----

[web]: screenshots/web.png "toujours la même erreur"
[alltests]: screenshots/alltests.png "les tests unitaires"
[walletapp]: screenshots/walletapp.png "l'application graphique"




